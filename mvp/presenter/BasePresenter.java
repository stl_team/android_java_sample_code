package com.generalmodule.basemodule.mvp.presenter;

import com.generalmodule.basemodule.BaseApplication;
import com.generalmodule.basemodule.api.ApiManager;

import io.reactivex.disposables.CompositeDisposable;

public class BasePresenter {
    ApiManager apiManager = BaseApplication.getAppComponent().provideApiManager();
    CompositeDisposable compositeDisposable = new CompositeDisposable();

}
