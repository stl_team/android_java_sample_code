package com.generalmodule.basemodule.mvp.presenter;


import com.generalmodule.basemodule.model.login.LoginModel;
import com.generalmodule.basemodule.mvp.contract.MainContract;

import io.reactivex.functions.Consumer;


public class MainPresenter extends BasePresenter {

    private MainContract mainView;

    public MainPresenter(MainContract mainView) {
        this.mainView = mainView;
    }


    public void apiLoginCall(String username, String password, String osFlag, String deviceId) {
        compositeDisposable.add(apiManager.login(username, password, osFlag, deviceId)
                .subscribe(new Consumer<LoginModel>() {
                    @Override
                    public void accept(LoginModel loginModel) {
                        mainView.OnSuccessLogin(loginModel);
                        // Toast.makeText(MainActivity.this, "accept", Toast.LENGTH_SHORT).show();

                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) {
                        throwable.printStackTrace();
                        mainView.onFailLogin(throwable);
                    }
                }));
    }

    public void reset(){
        compositeDisposable.clear();
    }
}
