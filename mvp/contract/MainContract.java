package com.generalmodule.basemodule.mvp.contract;

import com.generalmodule.basemodule.model.login.LoginModel;

public interface MainContract extends BaseContract {
    void callLoginApi(String username, String password, String osFlag, String deviceId);

    void OnSuccessLogin(LoginModel loginModel);

    void onFailLogin(Throwable throwable);

}
