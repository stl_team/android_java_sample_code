package com.generalmodule.basemodule.di.module;

import android.app.Application;

import com.generalmodule.basemodule.BaseApplication;
import com.generalmodule.basemodule.ui.base.BaseActivity;
import com.generalmodule.basemodule.api.ApiInterface;
import com.generalmodule.basemodule.api.ApiManager;
import com.generalmodule.basemodule.utils.FileUtils;
import com.generalmodule.basemodule.utils.NetworkUtils;
import com.generalmodule.basemodule.utils.common.FontCache;
import com.generalmodule.basemodule.utils.permission.PermissionUtil;
import com.generalmodule.basemodule.utils.PrefUtils;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 *
 *
 */
@Module
public class AppModule {

    private BaseApplication mApplication;


    public AppModule(BaseApplication application) {
        mApplication = application;
    }

    @Provides
    @Singleton
    Application providesApplication() {
        return mApplication;
    }

    // Dagger will only look for methods annotated with @Provides
    @Provides
    @Singleton
    // Application reference must come from AppModule.class
    PrefUtils providesSharedPreferences() {
        return new PrefUtils();
    }

    @Provides
    @Singleton
    BaseActivity provideBaseAppActivity(BaseActivity baseActivity) {
        return baseActivity;
    }

    @Provides
    @Singleton
    ApiManager provideApiManager(ApiInterface apiInterface) {
        return new ApiManager(apiInterface);
    }

    @Provides
    @Singleton
    FileUtils provideFileUtils() {
        return new FileUtils();
    }

    @Provides
    @Singleton
    NetworkUtils provideNetworkUtils() {
        return new NetworkUtils(mApplication.getApplicationContext());
    }

    @Provides
    @Singleton
    PermissionUtil providePermissionUtil() {
        return new PermissionUtil();
    }

    @Provides
    @Singleton
    FontCache provideFontCache() {
        return new FontCache(mApplication.getApplicationContext());
    }
}
