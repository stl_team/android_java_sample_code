package com.generalmodule.basemodule.di.module;

import android.content.Context;
import android.view.inputmethod.InputMethodManager;

import com.generalmodule.basemodule.api.ApiInterface;
import com.generalmodule.basemodule.di.scopes.ViewScope;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 *
 *
 */
@Module
public class TwitterModule {

    @Provides
    @Singleton
    ApiInterface provideRetrofit(Retrofit retrofit) {
        return retrofit.create(ApiInterface.class);
    }

    @ViewScope
    @Provides
    InputMethodManager provideInputMethodManager(Context context) {
        return (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
    }

}
