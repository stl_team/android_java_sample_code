package com.generalmodule.basemodule.di.component;

import com.generalmodule.basemodule.ui.base.BaseActivity;
import com.generalmodule.basemodule.api.ApiManager;
import com.generalmodule.basemodule.api.RetrofitInterceptor;
import com.generalmodule.basemodule.di.module.AppModule;
import com.generalmodule.basemodule.di.module.NetworkModule;
import com.generalmodule.basemodule.di.module.TwitterModule;
import com.generalmodule.basemodule.utils.FileUtils;
import com.generalmodule.basemodule.utils.NetworkUtils;
import com.generalmodule.basemodule.utils.common.FontCache;
import com.generalmodule.basemodule.utils.permission.PermissionUtil;
import com.generalmodule.basemodule.utils.PrefUtils;

import javax.inject.Singleton;

import dagger.Component;

/**
 *
 *
 */
@Singleton
@Component(modules = {AppModule.class, NetworkModule.class, TwitterModule.class})
public interface AppComponent {

    void inject(BaseActivity activity);

//    void inject(BaseModel baseModel);

    void inject(RetrofitInterceptor interceptor);

    ApiManager provideApiManager();

    PrefUtils providePrefUtil();

    FileUtils provideFileUtils();

    NetworkUtils provideNetworkUtils();

    PermissionUtil providePermissionUtil();

    FontCache provideFontCache();
}
