package com.generalmodule.basemodule.di.scopes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created on 6/30/2017.
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface ViewScope {
}