package com.generalmodule.basemodule.api;

import android.util.Log;

import com.generalmodule.basemodule.BaseApplication;
import com.generalmodule.basemodule.model.base.BaseModel;
import com.generalmodule.basemodule.model.base.CustomError;
import com.generalmodule.basemodule.model.login.LoginModel;
import com.generalmodule.basemodule.utils.NetworkUtils;
import com.generalmodule.basemodule.utils.common.ApiConstants;
import com.generalmodule.basemodule.utils.common.AppConstant;

import java.net.ConnectException;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import io.reactivex.ObservableTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

@Singleton
public class ApiManager {
    private ApiInterface apiInterface;
    private final NetworkUtils mNetworkUtils = BaseApplication.getAppComponent().provideNetworkUtils();

    @Inject
    public ApiManager(ApiInterface apiInterface) {
        this.apiInterface = apiInterface;
    }


    private <T> Observable<T> call(Observable<T> modelObservable) {

        return modelObservable
                .startWith(Observable.defer(() -> {
                    //before calling each api, network connection is checked.
                    if (!mNetworkUtils.isConnected()) {
                        //if network is not available, it will return error observable with ConnectException.
                        return Observable.error(new ConnectException("Device is not connected to network"));
                    } else {
                        //if it is available, it will return empty observable. Empty observable just emits onCompleted() immediately
                        return Observable.empty();
                    }
                }))
                .flatMap(response -> {
                    if (response instanceof BaseModel) {
                        BaseModel baseResponse = (BaseModel) response;
                        if (baseResponse.getStatus() != ApiConstants.ResponseCode.RESPONSE_SUCCESS) {
//                            showError(addOfficeResponse.getMessage());
                            CustomError customApiError = new CustomError(baseResponse.getCode(), baseResponse.getMessage());
                            return Observable.error(customApiError);
                        }
                        return Observable.just(response);
                    }
                    return Observable.just(response);
                })
                .doOnNext(response -> {
                    //logging response on success
                    //you can change to to something else
                    //for example, if all your apis returns error codes in success, then you can throw custom exception here
                    if (AppConstant.IS_DEBUGGABLE) {
                        Timber.e("Response :\n" + response);
                    }
                })
                .doOnError(throwable -> {
                    //printing stack trace on error
                    if (AppConstant.IS_DEBUGGABLE) {
                        throwable.printStackTrace();
                    }
                });


    }

    private <T> ObservableTransformer<T, T> applySchedulers() {
        return observable ->
                observable.subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

    }

    public Observable<LoginModel> login(String jabalpur, String s, String s1, String s2) {
        return call(apiInterface.login(jabalpur, s, s1, s2)).compose(applySchedulers());
//        return apiInterface.login(jabalpur, s, s1, s2);
    }


}
