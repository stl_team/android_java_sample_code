package com.generalmodule.basemodule.api;


import com.generalmodule.basemodule.model.login.LoginModel;
import com.generalmodule.basemodule.utils.common.ApiConstants;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * list of api goes here
 */

public interface ApiInterface {

    @FormUrlEncoded
    @POST(ApiConstants.EndPoints.USER_LOGIN)
    Observable<LoginModel> login(@Field(ApiConstants.params.USER_NAME) String uname,
                                 @Field(ApiConstants.params.PASSWORD) String password,
                                 @Field(ApiConstants.params.OS_FLAG) String osflag,
                                 @Field(ApiConstants.params.DEVICE_ID) String deviceid);
}
